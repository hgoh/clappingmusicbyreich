#!/usr/bin/env python 
###########################################################
## A python arrangement of Clapping Music by Steve Reich ##
###########################################################
__author__ = "Heedong Goh"                               ##
__email__ = "heedong.goh@utexas.edu"                     ##
###########################################################
import time
import os

# Input parameters
rhythm = [1,1,1,0,1,1,0,1,0,1,1,0]
beatsPerMinute = 360
pitch = 440
repPerRhythm = 8
silence = 8

# Generated parameters
bfreq = 60.0/beatsPerMinute
duration  = 0.5*bfreq
lenRhythm = len(rhythm)

# Begin performace
print()
print("##################################################################")
print("## A python arrangement of Clapping Music by Steve Reich (1972) ##")
print("##          arrangement by Heedong Goh <heedong.goh@utexas.edu> ##")
print("##################################################################")
print()
print()

time.sleep(1)
print("(Two performers enter the stage.)")
time.sleep(1)
print("(Audiences begin to applaud.)")
time.sleep(silence)
print("(Some unexpectedly long slience to draw attentions ...)")
time.sleep(silence)
print("(The performance begins.)")
time.sleep(1)

shift = 0
for iter in range(lenRhythm+1):
    for subIter in range(repPerRhythm):
        for loc in range(lenRhythm):
            beatA = rhythm[loc]
            beatB = rhythm[loc+shift]
            if beatA == 1 and beatB == 1:
                print("* *")
                os.system('play -nq -t alsa synth {} sine {}'.format(duration,pitch))
            elif beatA == 1 and beatB == 0:
                print("*  ")
                os.system('play -nq -t alsa synth {} sine {}'.format(duration,pitch*1.5))
            elif beatA == 0 and beatB == 1:
                print("  *")
                os.system('play -nq -t alsa synth {} sine {}'.format(duration,pitch/1.5))
            else:
                print("   ")
            time.sleep(bfreq)
    shift = shift - 1
    
exit()
